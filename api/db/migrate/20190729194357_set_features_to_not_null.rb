# frozen_string_literal: true

class SetFeaturesToNotNull < ActiveRecord::Migration[5.2]
  def change
    change_column_null(:mushrooms, :features, false)
  end
end
