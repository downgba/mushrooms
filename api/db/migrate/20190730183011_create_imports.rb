# frozen_string_literal: true

class CreateImports < ActiveRecord::Migration[5.2]
  def change
    create_table :imports do |t|
      t.string :file_md5, null: false, index: true
      t.timestamps
    end
  end
end
