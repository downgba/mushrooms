# frozen_string_literal: true

class CreateMushrooms < ActiveRecord::Migration[5.2]
  def change
    create_table :mushrooms do |t|
      t.jsonb :attributes

      t.timestamps
    end
  end
end
