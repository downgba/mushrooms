# frozen_string_literal: true

module Base
  class Mutation < GraphQL::Schema::Mutation; end
end
