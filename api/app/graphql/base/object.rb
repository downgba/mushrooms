# frozen_string_literal: true

module Base
  class Object < GraphQL::Schema::Object
    connection_type_class Connection
  end
end
