# frozen_string_literal: true

module Base
  class Input < GraphQL::Schema::InputObject
    argument_class Base::Argument
  end
end
