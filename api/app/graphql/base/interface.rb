# frozen_string_literal: true

module Base
  module Interface
    include GraphQL::Schema::Interface
  end
end
