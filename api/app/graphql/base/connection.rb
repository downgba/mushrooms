# frozen_string_literal: true

module Base
  class Connection < GraphQL::Types::Relay::BaseConnection
    field :total_count, Integer, null: false

    def total_count
      object.nodes.count
    end
  end
end
