# frozen_string_literal: true

module Base
  class Union < GraphQL::Schema::Union; end
end
