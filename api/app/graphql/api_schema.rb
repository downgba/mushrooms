# frozen_string_literal: true

class ApiSchema < GraphQL::Schema
  mutation(Types::Mutation)
  query(Types::Query)
end
