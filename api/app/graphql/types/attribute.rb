# frozen_string_literal: true

module Types
  class Attribute < Base::Object
    graphql_name "Attribute"

    field :name, String, "Attribute name E.g.: classes", null: false
    field :value, String, "Attribute value E.g.: edible", null: false

    def name
      object.first
    end

    def value
      object.last
    end
  end
end
