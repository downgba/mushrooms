# frozen_string_literal: true

module Types
  class Mushroom < Base::Object
    graphql_name "Mushroom"

    field :attributes, [Attribute], "List of attributes", null: false

    def attributes
      object.features.map
    end
  end
end
