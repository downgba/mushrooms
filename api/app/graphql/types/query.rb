# frozen_string_literal: true

module Types
  class Query < Base::Object
    field :attributes, [Attributes], "List of possible attributes", null: false

    field :mushrooms, Mushroom.connection_type, null: true, description: "List of mushrooms" do
      argument :attributes, [Inputs::Attribute], "Filter by the given attributes", required: false
    end

    def mushrooms(attributes: [])
      dataset = ::Mushroom.all

      attributes.each do |attribute|
        dataset = dataset.where("features->'#{attribute[:name]}' ? :value", value: attribute[:value])
      end

      dataset
    end

    def attributes
      ::Attributes::MAP.map
    end
  end
end
