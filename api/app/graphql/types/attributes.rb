# frozen_string_literal: true

module Types
  class Attributes < Base::Object
    graphql_name "Attributes"

    field :name, String, "Attribute name", null: false
    field :options, [String], "List of possible values", null: false

    def name
      object.first
    end

    def options
      object.last.values
    end
  end
end
