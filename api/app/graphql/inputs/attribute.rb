# frozen_string_literal: true

module Inputs
  class Attribute < Base::Input
    graphql_name "AttributeInput"

    argument :name, String, "Attribute name E.g.: classes", required: true
    argument :value, String, "Attribute value E.g.: edible", required: true
  end
end
