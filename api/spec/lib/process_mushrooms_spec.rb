require "rails_helper"

RSpec.describe ProcessMushrooms do
  describe ".call" do
    subject(:call) { described_class.call(file: file) }

    let(:file) { double(:file, path: "path") }
    let(:digest_md5) { double(:digest_md5, hexdigest: "test") }
    let(:content) do
      ["e", "x", "s", "n", "f", "n", "a", "c", "b", "y", "e", "?", "s", "s", "o", "o", "p", "o", "o", "p", "o", "c", "l"]
    end

    before do
      allow(CSV).to receive(:foreach).and_yield(content)
      allow(Digest::MD5).to receive(:file) { digest_md5 }
    end

    it "creates a mushroom" do
      expect { call }.to change { Mushroom.count }.from(0).to(1)
      expect(Import.count).to eq(1)
      expect(Mushroom.last.features).to eq(
        "classes" => "edible",
        "cap-shape" => "convex",
        "cap-surface" => "smooth",
        "cap-color" => "brownn",
        "bruises?" => "no",
        "odor" => "none",
        "gill-attachment" => "attached",
        "gill-spacing" => "close",
        "gill-size" => "broad",
        "gill-color" => "yellow",
        "stalk-shape" => "enlarging",
        "stalk-root" => nil,
        "stalk-surface-above-ring" => "smooth",
        "stalk-surface-below-ring" => "smooth",
        "stalk-color-above-ring" => "orange",
        "stalk-color-below-ring" => "orange",
        "veil-type" => "partial",
        "veil-color" => "orange",
        "ring-number" => "one",
        "ring-type" => "pendant",
        "spore-print-color" => "orange",
        "population" => "clustered",
        "habitat" => "leaves"
      )
    end

    context "when file has already been imported" do
      let!(:import) { Import.create(file_md5: "test") }

      it "does not import the file" do
        expect { call }.not_to change(Mushroom, :count)
      end
    end
  end
end
