# frozen_string_literal: true

require "csv"
require "benchmark"
require "activerecord-import"

class ProcessMushrooms < BaseInteractor
  BATCH_SIZE = 200

  def initialize(file:)
    @file = file
    @md5 = Digest::MD5.file(@file).hexdigest
  end

  def call
    logger.info("Starting processing #{@file.path}")

    return logger.info("*** Skipping - File has already been imported ***") if processed?

    time = Benchmark.realtime { process_file }

    logger.info("Time elapsed #{time * 1000} milliseconds")
    logger.info("Done!")
  end

  private

  def process_file
    mushrooms = []

    CSV.foreach(@file) do |line|
      mushroom_attributes = {}
      Attributes::MAP.each_with_index do |attributes, index|
        name, options = attributes
        mushroom_attributes[name] = options[line[index].to_sym]
      end

      mushrooms << { features: mushroom_attributes }

      if mushrooms.size > BATCH_SIZE
        store(mushrooms)
        mushrooms = []
      end
    end

    store(mushrooms)
    Import.create(file_md5: @md5)
  end

  def store(mushrooms)
    Mushroom.import(mushrooms, validate: false)
  end

  def processed?
    Import.where(file_md5: @md5).exists?
  end

  def logger
    Rails.logger
  end
end
