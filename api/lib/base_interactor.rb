# frozen_string_literal: true

class BaseInteractor
  def initialize(**args); end

  def call
    raise NotImplementedError
  end

  def self.call(**args)
    new(args).call
  end
end
