# frozen_string_literal: true

namespace :mushrooms do
  desc "Import mushrooms from the given file"
  task :import, [:file] => :environment do |_, args|
    ProcessMushrooms.call(file: File.open(args[:file]))
  rescue Errno::ENOENT
    Rails.logger.warn("File: #{args[:file]} not found")
  end
end
