## Mushrooms

### Instructions 

Clone the repository   

```
	git clone git@bitbucket.org:downgba/mushrooms.git
	cd mushrooms
```

Create the database   

```
	docker-compose run api sh -c "rake db:create && rake db:migrate"
```

Import mushrooms   

```
	docker-compose run api sh -c "rake mushrooms:import[data/mushrooms.data]"
```

Start app

```
	docker-compose up
```

Access http://localhost:3000/

### Running tests

```
	docker-compose run -e RAILS_ENV=test api sh -c "rspec"
```