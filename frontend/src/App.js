import React, { Component } from 'react'
import Dropdown from './Dropdown'
import axios from 'axios'

class App extends Component {
  constructor(){
    super()

    axios.post(process.env.REACT_APP_API_URL, {
      query: "query { attributes { name options } }"
    }).then(response => {
      this.setState({ attributes: response.data.data.attributes })
    })

    this.handleChanges = this.handleChanges.bind(this)

    this.search()
  }

  state = {
    attributes: [],
    filters: {},
    total: 0
  }

  handleChanges(name, value){
    let filters = this.state.filters
    filters[name] = value

    this.setState({ filters: filters })
  }

  reset(){
    let filters = this.state.filters
    this.state.attributes.forEach(function(item){
      document.getElementById(item.name).value = ""
      delete filters[item.name]
    })

    this.setState({ filters: filters })
    this.search()
  }

  search(){
    let keys = Object.getOwnPropertyNames(this.state.filters)
    let filters = this.state.filters
    let attributes = []

    keys.forEach(function(key){
      let value = filters[key]
      if(value === "") return

      attributes.push({ name: key, value: value })
    })

    axios.post(process.env.REACT_APP_API_URL, {
      query: "query Mushrooms($attributes: [AttributeInput!]){ mushrooms(attributes: $attributes) { totalCount } }",
      variables: { attributes: attributes }
    }).then(response => {
      this.setState({ total: response.data.data.mushrooms.totalCount })
    })
  }

  render(){
    return (
      <div className="App">
        <div id="filters" name="filters" className="mushrooms-filters">
          {
            this.state.attributes.map(attribute => (
              <Dropdown key={attribute.name} handleChanges={this.handleChanges} name={attribute.name} options={attribute.options} />
            ))
          }
        </div>

        <div id="actions" className="actions">
          <button key="reset" onClick={this.reset.bind(this)}>Reset</button>
          <button key="search" onClick={this.search.bind(this)}>Search</button>
        </div>

        <div id="results" className="results">
          <div>Total Results: {this.state.total}</div>
        </div>

      </div>
    );
  }
}

export default App
