import React, { Component } from 'react'

class Dropdown extends Component {
  handleChanges(event){
    event.preventDefault()
    this.props.handleChanges(event.target.name, event.target.value)
  }

  render(){
    return (
      <div>
        <label>{this.props.name}</label>
        <select id={this.props.name} name={this.props.name} onChange={this.handleChanges.bind(this)}>
          <option value="" defaultValue>All</option>
          {
            this.props.options.map((item, index) => (
              <option key={item} value={item}>{item}</option>
            ))
          }
        </select>
      </div>
    )
  }
}

export default Dropdown
